importScripts('https://www.gstatic.com/firebasejs/7.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.8.1/firebase-messaging.js');

firebase.initializeApp({
  apiKey: 'AIzaSyCsqjX1ShOmFytkoN98IwGr0CmDjv05DTM',
  authDomain: 'padel-daaca.firebaseapp.com',
  databaseURL: 'https://padel-daaca.firebaseio.com',
  projectId: 'padel-daaca',
  storageBucket: 'gs://padel-daaca.appspot.com',
  messagingSenderId: '224313500198',
  appId: '1:224313500198:web:675f601264623e3abc7ce7'
});

const messaging = firebase.messaging();

//curl https://fcm.googleapis.com/fcm/send --header "Authorization:key=AAAANDogYiY:APA91bH8IN-Rt6XkMq0nYKMzd53_OKUfDwzCqYX7fNFwcvz-jgNAo1HG9bBnqrkYFZkYQn275g0vjGVaUVWt3ERZ5jOaHBALUM_IuAwFYssURBxEnKI8Kn2voEMc6sx4LmYlFeyq_-pB" --header "Content-Type:application/json" -d '{ "notification": { "title": "Hey amigo, lee esto!", "body": "Felicidades!! Has recibido una gloriosa notificación", "icon": "/notificaciones/images/user-icon.png" }, "to" : "eHBdLMIunbunpAfb-JLhoQ:APA91bG44E9bQmxVuOaJGK3fIfSCOFZIgq7OUmu26BHQotBCl8o1v9PqMuSj5ACeeLJlpxqNqgghI9DH1ratIh5jvc2s-gyhv2REwCSDh8ATTtn6026qiQ7nsDUnC99zVvzzbiZfTiZD" }'

// messaging.setBackgroundMessageHandler(function(payload) {
//   console.log(
//     '[firebase-messaging-sw.js] Received background message ',
//     payload
//   );
//   // Customize notification here
//   const notificationTitle = 'Background Message Title';
//   const notificationOptions = {
//     body: 'Background Message body.',
//     icon: '/firebase-logo.png'
//   };

//   return self.registration.showNotification(
//     notificationTitle,
//     notificationOptions
//   );
// });
