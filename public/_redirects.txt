[[redirects]]
  from = "/*"
  to = "/"
  status = 404
  force = false
  query = {path = ":path"}
  conditions = {Language = ["es"], Country = ["SP"], Role = ["admin"]}
