import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import firebase from 'firebase';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
  created() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCsqjX1ShOmFytkoN98IwGr0CmDjv05DTM',
      authDomain: 'padel-daaca.firebaseapp.com',
      databaseURL: 'https://padel-daaca.firebaseio.com',
      projectId: 'padel-daaca',
      storageBucket: 'gs://padel-daaca.appspot.com',
      messagingSenderId: '224313500198',
      appId: '1:224313500198:web:675f601264623e3abc7ce7',
    });

    // var messaging = firebase.messaging();
    // messaging
    //   .requestPermission()
    //   .then(function() {
    //     console.log('Se han aceptado las notificaciones');
    //     return messaging.getToken();
    //   })
    //   .then(function(token) {
    //     if (token) {
    //       store.state.token = token;
    //     } else {
    //       store.state.token = '';
    //     }
    //   })
    //   .catch(function(err) {
    //     console.log('No se ha recibido permiso / token: ', err);
    //   });

    store.dispatch('findData');
  },
}).$mount('#app');

// Initialize Firebase
