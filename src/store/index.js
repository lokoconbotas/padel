import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import * as firebase from 'firebase';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data: null,
  },
  mutations: {
    setData(state, payload) {
      state.data = payload;
    },
  },
  actions: {
    async findData(context) {
      let call = await fetch('https://padel-daaca.firebaseio.com/.json');
      let data = await call.json();

      let jugadores = Object.values(data.jugadores);
      let partidos = Object.values(data.partidos);

      let datosConPartidos = [];
      let conTotalPuntos = [];

      if (partidos.length > 0) {
        datosConPartidos = jugadores.map((j) => {
          return { ...j, partidos: partidosPlayer(j, partidos) };
        });
        conTotalPuntos = datosConPartidos.map((j) => {
          return {
            ...j,
            total: totalPuntos(j),
            total2: totalPuntos2(j),
            totalFallos: totalFallos(j),
            mediaAciertos: mediaAciertos(j),
            mediaFallos: mediaFallos(j),
            ganados: totalGanados(j),
            perdidos: totalPerdidos(j),
          };
        });
      } else {
        datosConPartidos = jugadores.map((j) => {
          return { ...j, partidos: partidosPlayer(j, partidos) };
        });
        conTotalPuntos = datosConPartidos.map((j) => {
          return { ...j, total: 0 };
        });
      }

      context.commit('setData', conTotalPuntos);
    },
  },
  getters: {
    getData(state) {
      return state.data;
    },
  },
  modules: {},
});

function total2(partido) {
  let puntosGanador = 2;

  if (partido.winner) {
    puntosGanador = 5;
  }

  return puntosGanador;
}
function total(partido) {
  let puntosGanador = 0;

  if (partido.winner) {
    puntosGanador = 15;
  }

  return Number(partido.puntos * 3) - Number(partido.fallos) + puntosGanador;
}

function partidosPlayer(player, partidos) {
  let final = [];
  partidos.forEach((p) => {
    if (p.stats.map((s) => s.jugador).includes(player.nombre)) {
      let obj = {};
      let data = p.stats.find((s) => s.jugador == player.nombre);
      obj.fecha = p.fecha;
      obj.fallos = data.fallos;
      obj.puntos = data.puntos;
      obj.winner = data.winner;
      final.push(obj);
    }
  });

  return final;
}

function totalPuntos(player) {
  if (player.partidos.length > 0) {
    return player.partidos
      .map((a) => total(a))
      .reduce((a, b) => Number(a) + Number(b), 0);
  } else {
    return 0;
  }
}
function totalPuntos2(player) {
  if (player.partidos.length > 0) {
    return player.partidos
      .map((a) => total2(a))
      .reduce((a, b) => Number(a) + Number(b), 0);
  } else {
    return 0;
  }
}

function totalFallos(player) {
  if (player.partidos.length > 0) {
    return player.partidos
      .map((a) => a.fallos)
      .reduce((a, b) => Number(a) + Number(b), 0);
  } else {
    return 0;
  }
}

function mediaAciertos(player) {
  if (player.partidos.length > 0) {
    return (
      player.partidos
        .map((a) => a.puntos)
        .reduce((a, b) => Number(a) + Number(b), 0) / player.partidos.length
    ).toFixed(0);
  } else {
    return 0;
  }
}

function mediaFallos(player) {
  if (player.partidos.length > 0) {
    return (
      player.partidos
        .map((a) => a.fallos)
        .reduce((a, b) => Number(a) + Number(b), 0) / player.partidos.length
    ).toFixed(0);
  } else {
    return 0;
  }
}

function totalGanados(player) {
  if (player.partidos.length > 0) {
    return player.partidos.filter((a) => a.winner).length;
  } else {
    return 0;
  }
}
function totalPerdidos(player) {
  if (player.partidos.length > 0) {
    return player.partidos.filter((a) => !a.winner).length;
  } else {
    return 0;
  }
}

function sortedPartidos(partidos) {
  return partidos.sort((a, b) =>
    a.fecha > b.fecha ? 1 : b.fecha > a.fecha ? -1 : 0
  );
}
